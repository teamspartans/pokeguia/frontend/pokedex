import React from 'react'
import { Container, Img, Card, Pokeball } from './styles'

import PokeballIcon from '../../assets/static/pokeballGold.svg'
import { useNearScreen } from '../../hook/useNearScreen'

export const LegendaryCard = (props) => {
  const {
    name = 'mew',
    image = 'https://vignette.wikia.nocookie.net/es.pokemon/images/b/bf/Mew.png/revision/latest/scale-to-width-down/1000?cb=20160311010530',
    pokedexNumber = 151,
    handleClick
  } = props

  const [show, ref] = useNearScreen()

  const handleLegendary = () => {
    handleClick({ pokedexNumber: pokedexNumber })
  }

  return (
    <Container tabIndex='0' role='listitem' aria-label='List item' ref={ref} onClick={handleLegendary}>
      {show &&
        <>
          <Img tabIndex='-1' role='figure' aria-label='Pokemon preview'>
            <img
              src={image}
              alt={`Legendary pokemon #${pokedexNumber}`}
            />
          </Img>

          <Card>
            <h3>{name}</h3>
            <Pokeball>
              <img src={PokeballIcon} alt='golden pokeball icon' />
            </Pokeball>
          </Card>
        </>}
    </Container>
  )
}
