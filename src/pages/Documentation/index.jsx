/* eslint-disable no-irregular-whitespace */
import React from 'react'
import { StylesDocumentation, Container } from './styles'
import colors from '../../assets/static/colorsUIKit.png'
import entityDiagram from '../../assets/static/erDiagram.png'
import deployDiagram from '../../assets/static/deployDiagram.png'
import crispFlowDiagram from '../../assets/static/crispFlow.png'
import threshAcurrayDiagram from '../../assets/static/threshAndAccuray.png'

export const Documentation = () => {
  return (
    <StylesDocumentation>
      <Container>
        <h1>Documentatiton</h1>
        <p>
            This project is the mixture of a lot of effort and love between the
            team members. The project was built in 4 blocks: frontend design,
            backend and data science, we leave you the documentation that we
            generate so that it serves as inspiration and you know how the project
            was built. If you have any questions or comments you want to make, you
            can write to us in our respective emails or social networks
        </p>
        <hr />
        <h2>Design</h2>
        <h3>Invision</h3>
        <p>
            I sketch the project in invision sharing ideas to develop an intuitive
            and youthful platform
          <br />
          <a
            href='https://emmanuelgarca781790.invisionapp.com/freehand/Pokedex--9muF4I1xK'
            target='_blank'
            rel='noopener noreferrer'
          >See the project in Invision
          </a>
        </p>
        <h3>Wireframes</h3>
        <p>
            Medium and high fidelity wireframes were generated on an atomic design
            in <strong>Figma</strong>
          <br />
          <a
            href='https://www.figma.com/file/QegU0rGFO3lHoGKOFPtAer/Pokedex?node-id=2%3A3'
            target='_blank'
            rel='noopener noreferrer'
          >See the project in Figma
          </a>
          <br />
          <a href='https://www.behance.net/gallery/99553929/Pokedex-Find-your-favorite-pokemon' target='_blank' rel='noopener noreferrer'>Watch the project presentation at Behance
          </a>
        </p>
        <hr />
        <h2>Frontend</h2>
        <p>
            The page is built 100% responsive for desktop, tablet and mobile
            devices with the best practices in the code and thinking about the
            accessibility of all users. We use
          <strong>
            <a
              href='https://reactjs.org/'
              target='_blank'
              rel='noopener noreferrer'
            >React
            </a>
          </strong>
            , a famous and widely accepted in the developer community for it stability and power.
        </p>
        <h3>Color palette</h3>
        <p>
            The color palette was created thinking that no disabled person is
            prevented from seeing legibly
        </p>
        <img src={colors} alt='colors used for the portal ' />
        <small>https://color.adobe.com/create/color-accessibility</small>
        <h3>Fonts</h3>
          Modern, rounded fonts were used to create a youthful atmosphere considering that it is easy for users to read<br />
        <a
          href='https://fonts.google.com/specimen/Karla?query=karla'
          target='_blank'
          rel='noopener noreferrer'
        >Karla font:
        </a>
        <br />
        <a
          href='https://fonts.google.com/specimen/Source+Sans+Pro?query=source'
          target='_blank'
          rel='noopener noreferrer'
        >Source Sans Pro
        </a>
        <hr />
        <h2>Backend</h2>
        <p>Data exposition from an API built with GraphQL to make more complete and specific queries.</p>
        <h3>Diagram</h3>
        <img src={deployDiagram} alt='Deploy diagram' />
        <h3>API</h3>
        <p>API for detailed query of pokemon</p>
        <ul>
          <li><p><strong>GraphQL: </strong>is an open-source data query and manipulation language for APIs, and a runtime for fulfilling queries with existing data.</p></li>
          <li><p><strong>Graphene-Django: </strong>is built on top of Graphene. Graphene-Django provides some additional abstractions that make it easy to add GraphQL functionality to your Django project.</p></li>
        </ul>
        <p>Check the API and documentation <a href='https://blissful-mile-280405.ue.r.appspot.com/graphql' target='_blank' rel='noopener noreferrer'>here</a></p>
        <h3>Database</h3>
        <ul>
          <li><p><strong>Postgresql database: </strong>PostgreSQL is a powerful, open source object-relational database system with over 30 years of active development that has earned it a strong reputation for reliability, feature robustness, and performance.</p></li>
        </ul>
        <i>Entity relationship diagram</i>
        <img src={entityDiagram} alt='Entity diagram' />
        <h3>Django</h3>
        <p>Django is a high-level Python Web framework that encourages rapid development and clean, pragmatic design. Built by experienced developers, it takes care of much of the hassle of Web development, so you can focus on writing your app without needing to reinvent the wheel. It’s free and open source.</p>
        <p>Ridiculously fast.</p>
        <ul>
          <li>Separate environments
            <ul>
              <li>Development</li>
              <li>Testing</li>
              <li>Production</li>
            </ul>
          </li>
        </ul>
        <hr />
        <h2>Datascience</h2>
        <h3>Methodology</h3>
        <p>CRIPS-DM (Cross-Industry Standard Process for Data Mining)</p>
        <img src={crispFlowDiagram} alt='CRISP FLOW diagram' />
        <h3>Business Understanding</h3>
        <p>The results of the search to understand the business are found <a href='https://www.notion.so/pokespartans/An-lisis-y-entendimiento-del-problema-810ad9c60906493ab4f5f3b213036b86' target='_blank' rel='noopener noreferrer'>here</a></p>
        <h3>Data Understanding</h3>
        <p>Data Exploration: <a href='https://gitlab.com/pokespartans/datascience/data-exploration-analysis-and-treatment/-/blob/master/Notebooks/Data_Exploration.ipynb' target='_blank' rel='noopener noreferrer'>Link</a></p>
        <h3>Data Preparation</h3>
        <p>Clean Data: <a href='https://gitlab.com/pokespartans/datascience/data-exploration-analysis-and-treatment/-/blob/master/Notebooks/Clean_Data_Data_Export.ipynb' target='_blank' rel='noopener noreferrer'>Link</a></p>
        <p>Battle Simulations: <a href='https://gitlab.com/pokespartans/datascience/data-exploration-analysis-and-treatment/-/blob/master/Notebooks/Battle_Simulations.ipynb' target='_blank' rel='noopener noreferrer'>Link</a></p>
        <p>Data for Models: <a href='https://gitlab.com/pokespartans/datascience/data-exploration-analysis-and-treatment/-/blob/master/Notebooks/Dataset_With_Processed_Data_For_Models.ipynb' target='_blank' rel='noopener noreferrer'>Link</a></p>
        <p>Pokemon Skills: <a href='https://gitlab.com/pokespartans/datascience/data-exploration-analysis-and-treatment/-/blob/master/Notebooks/Pokemon_Strength.ipynb' target='_blank' rel='noopener noreferrer'>Link</a></p>
        <p>Pokemon Types: <a href='https://gitlab.com/pokespartans/datascience/data-exploration-analysis-and-treatment/-/blob/master/Notebooks/Type_catalogs_for_data_filters.ipynb' target='_blank' rel='noopener noreferrer'>Link</a></p>
        <h3>Features</h3>
        <h3>Feature generation</h3>
        <ul>
          <li><p>Strength:  Strength obtained from Pokemon <a href='https://gitlab.com/pokespartans/datascience/data-exploration-analysis-and-treatment/-/blob/master/Notebooks/Pokemon_Strength.ipynb' target='_blank' rel='noopener noreferrer'>Link</a></p></li>
          <li><p>Wins: Number of victories in simulated battles <a href='https://gitlab.com/pokespartans/datascience/data-exploration-analysis-and-treatment/-/blob/master/Notebooks/Battle_Simulations.ipynb' target='_blank' rel='noopener noreferrer'>Link</a></p></li>
          <li><p>Abilities number: amount of skills <a href='https://gitlab.com/pokespartans/datascience/data-exploration-analysis-and-treatment/-/blob/master/Notebooks/Pokemon_Skills.ipynb' target='_blank' rel='noopener noreferrer'>Link</a></p></li>
        </ul>
        <h3>Selection Features</h3>
        <p>it was found that to classify the legendary Pokemon, 7 characteristics were enough to obtain very good results.</p>
        <img src={threshAcurrayDiagram} alt='thresh and acurray diagram' />
        <p>7 sets of features were generated based on different selection technique:</p>
        <ul>
          <li>Sklearn y XGBoost</li>
          <li>SKlearn
            <ul>
              <li>Features with low variance</li>
              <li>Univariate feature</li>
              <li>L1-based feature</li>
              <li>Tree-based feature</li>
            </ul>
          </li>
        </ul>
        <p>See Also: <a href='https://gitlab.com/pokespartans/datascience/data-exploration-analysis-and-treatment/-/blob/master/Notebooks/IA_models_For_Classify_Legendary_Pokemons.ipynb#Sklearn%20and%20XGBoost' target='_blank' rel='noopener noreferrer'>Link</a></p>
        <h3>Models</h3>
        <p>Several models were trained with each of the 7 sets of characteristics</p>
        <ul>
          <li>XGBClassifier</li>
          <li>Linear SVC</li>
          <li>ExtraTreesClassifier</li>
          <li>DecisionTreeClassifier</li>
        </ul>
        <p>See Also: <a href='https://gitlab.com/pokespartans/datascience/data-exploration-analysis-and-treatment/-/blob/master/Notebooks/IA_models_For_Classify_Legendary_Pokemons.ipynb#IA%20Models' target='_blank' rel='noopener noreferrer'>Link</a></p>
        <h3>Results</h3>
        <p>Very good results were obtained, all models will have more than 90% Accuracy</p>
        <p>See Also: <a href='https://gitlab.com/pokespartans/datascience/data-exploration-analysis-and-treatment/-/blob/master/Notebooks/IA_models_For_Classify_Legendary_Pokemons.ipynb#IA%20Models' target='_blank' rel='noopener noreferrer'>Link</a></p>
        <hr />
        <h2>Notion</h2>
        <p>
            Notion has been a great tool to document our information and have
            agreements between colleagues. If you want to know more about how we
            organize the project, we leave you the link
        </p>
        <br />
        <a
          href='https://fonts.google.com/specimen/Source+Sans+Pro?query=source'
          target='_blank'
          rel='noopener noreferrer'
        >Link of the documentation in Notion
        </a>
      </Container>
    </StylesDocumentation>
  )
}
