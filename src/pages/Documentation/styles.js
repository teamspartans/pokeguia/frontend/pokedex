import styled from 'styled-components'

export const StylesDocumentation = styled.section`
display: flex;
flex-direction: column;
align-items: center;
justify-content: center;
background-color: ${({ theme }) => theme.colorGrey};
min-height: calc(100vh - 140px);
`
export const Container = styled.div`
width: 85%;
box-sizing: border-box;
margin: ${({ theme }) => theme.spacingB} auto;
max-width: 100vw;
margin: 0;
box-sizing: border-box;
& img {
width: 50%;
margin: 0;
}
& p,
a {
line-height: 150%;
font-size: ${({ theme }) => theme.sizeFontsH3};
}
& a {
text-transform: none;
color: rgb(36, 36, 106);
}
& h1 {
font-size: ${({ theme }) => theme.sizeFontTitle};
margin: ${({ theme }) => theme.spacingXL} 0;
color: rgb(36, 36, 106);
@media (max-width: 425px) {
font-size: ${({ theme }) => theme.sizeFontCardTitleImg};
}
}
& h2 {
margin: ${({ theme }) => theme.spacingXL} 0;
font-size: ${({ theme }) => theme.sizeFontSubtitle};
color: rgb(36, 36, 106);
}
& h3 {
margin: ${({ theme }) => theme.spacingL} 0;
font-size: ${({ theme }) => theme.sizeFontsH3};
color: rgb(36, 36, 106);
}
& hr {
margin: 70px 0;
background-color: rgb(36, 36, 106);
height: 3px;
}
`
